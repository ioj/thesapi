// Copyright © 2016 Mariusz Nowak <mariusz@nowak.wroclaw.pl>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"database/sql"
	"encoding/csv"
	"io"
	"os"
	"strings"

	// use sqlite3
	_ "github.com/mattn/go-sqlite3"
	"github.com/spf13/cobra"
)

func prepareDb(db *sql.DB, data io.Reader) error {
	logger := rootLog.New("import", "prepareDb")

	// check if the 'words' table exists
	var name string
	err := db.QueryRow(`
        SELECT name FROM sqlite_master
        WHERE type='table' AND name='words'
        `).Scan(&name)

	if err == sql.ErrNoRows {
		// create the table and index if it doesn't exist
		logger.Info("create table")
		_, err = db.Exec(`
            CREATE TABLE words (
                'id' INTEGER PRIMARY KEY,
                'group' INTEGER NOT NULL,
                'word' TEXT NOT NULL
            );
            CREATE INDEX words_idx ON words (word);
            `)
	}

	if err != nil {
		return err
	}

	// clear the table
	_, err = db.Exec("DELETE FROM words")
	if err != nil {
		return err
	}

	reader := csv.NewReader(data)
	reader.Comment = '#'
	reader.FieldsPerRecord = 2
	reader.Comma = ','
	reader.LazyQuotes = true

	logger.Info("insert values")

	tx, err := db.Begin()
	if err != nil {
		return err
	}

	stmt, err := tx.Prepare("INSERT INTO words ('group', word) VALUES (?, ?)")
	if err != nil {
		return err
	}

	rowsInserted := 0
	for {
		record, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			return err
		}

		if _, err = stmt.Exec(record[0], strings.ToLower(record[1])); err != nil {
			return err
		}

		rowsInserted++
		if rowsInserted%10000 == 0 {
			logger.Info("rows inserted", "rows", rowsInserted)
		}
	}

	tx.Commit()
	logger.Info("all done")

	return nil
}

// importCmd represents the import command
var importCmd = &cobra.Command{
	Use:   "import",
	Short: "Imports a source csv into a sqlite database",
	Run: func(cmd *cobra.Command, args []string) {
		logger := rootLog.New("import", nil)
		if len(args) != 1 {
			logger.Crit("Usage: thesapi import <filename.csv>")
			os.Exit(-1)
		}

		db, err := sql.Open("sqlite3", dbName)
		if err != nil {
			logger.Crit("Open database error", "err", err)
			os.Exit(-1)
		}
		defer db.Close()

		data, err := os.Open(args[0])
		if err != nil {
			logger.Crit("Open CSV error", "err", err)
			os.Exit(-1)
		}
		defer data.Close()

		if err := prepareDb(db, data); err != nil {
			logger.Crit("Import error", "err", err)
			os.Exit(-1)
		}
	},
}

func init() {
	RootCmd.AddCommand(importCmd)
}
