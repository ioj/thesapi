// Copyright © 2016 Mariusz Nowak <mariusz@nowak.wroclaw.pl>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"

	log "github.com/inconshreveable/log15"

	"github.com/spf13/cobra"
)

type Synonyms struct {
	Word     string   `json:"word"`
	Synonyms []string `json:"synonyms"`
}

var db *sql.DB
var logger log.Logger
var latency int

func handler(w http.ResponseWriter, r *http.Request) {
	if latency > 0 {
		sleep := float64(latency * 50)
		sleep = sleep + sleep*rand.Float64()
		time.Sleep(time.Duration(sleep) * time.Millisecond)
	}
	word := strings.Trim(strings.ToLower(strings.SplitN(r.URL.Path[1:], "/", 2)[0]), " \t")
	logger.Info(fmt.Sprintf("request %s", word), "method", r.Method, "remote", r.RemoteAddr)
	if len(word) == 0 {
		http.NotFound(w, r)
		return
	}

	rows, err := db.Query(`
        SELECT DISTINCT word FROM words
          WHERE "group" IN (
            SELECT "group" FROM words
            WHERE word = ?)
					AND word != ?
        `, word, word)

	if err != nil {
		logger.Error("Error during lookup", "err", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	synonyms := []string{}
	for rows.Next() {
		var synonym string
		rows.Scan(&synonym)
		synonyms = append(synonyms, synonym)
	}

	if len(synonyms) == 0 {
		http.NotFound(w, r)
		return
	}

	response := Synonyms{Word: word, Synonyms: synonyms}
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)
}

// serveCmd represents the serve command
var serveCmd = &cobra.Command{
	Use:   "serve",
	Short: "Run a Thesaurus API server",
	Run: func(cmd *cobra.Command, args []string) {
		var err error

		logger = rootLog.New("serve", nil)

		db, err = sql.Open("sqlite3", dbName)
		if err != nil {
			logger.Crit("Open database error", "err", err)
			os.Exit(-1)
		}
		defer db.Close()

		http.HandleFunc("/", handler)
		addr := cmd.Flag("bind").Value.String()
		logger.Info("start serving", "addr", addr)
		http.ListenAndServe(addr, nil)
	},
}

func init() {
	RootCmd.AddCommand(serveCmd)
	serveCmd.PersistentFlags().String("bind", ":7500", "address to bind")
	serveCmd.PersistentFlags().IntVarP(&latency, "latency", "l", 0, "random delay level for request (0 - none, 9 - quite high)")
}
